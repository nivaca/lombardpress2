class ApplicationMailer < ActionMailer::Base
  default from: "admin@lombardpress.org"
  
  layout 'mailer'
 
end
